# test_portfolio.py
import unittest
from portfolio import Portfolio

class PortfolioTestCase(unittest.TestCase):
    def assertCostEqual(self, p, cost):         
        self.assertEqual(p.cost(), cost)

class PortfolioTest(PortfolioTestCase):
    def setUp(self):
        self.p = Portfolio()
        self.p.buy("Microsoft", 100, 100.)
        self.p.buy("IBM", 40, 30.)
        self.p.buy("Google", 150, 99.99)
    
    def test_empty(self):
        self.assertCostEqual(Portfolio(), 0.0)
  
    def test_buy(self):
        self.assertCostEqual(self.p, 26198.5)
    
    def test_sell(self):
        self.p.sell('IBM', 3)
        self.assertCostEqual(self.p, 26108.5)
    
    def test_not_enough(self):
        with self.assertRaises(ValueError):
            self.p.sell("IBM", 100)

    def test_dont_own_stock(self):
        with self.assertRaises(ValueError):
            self.p.sell("NN Group", 100)

    def test_repr(self):
        self.assertIsInstance(repr(self.p), str)
        self.assertIn(str(self.p.cost()), repr(self.p))




if __name__ == "__main__":
    unittest.main()
