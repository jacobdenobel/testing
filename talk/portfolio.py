# portfolio.py
from typing import List
from dataclasses import dataclass

@dataclass
class Stock:
    name: str
    shares: int
    price: float


class Portfolio:
    'Simple stock portfolio class'

    def __init__(self, stocks: List[Stock] = None) -> None:
        self.stocks = stocks or []
    
    def buy(self, name:str, shares:int, price:float) -> None:
        'Creates a single Stock, appends it to self.stocks'
        self.stocks.append(Stock(name, shares, price))

    def cost(self) -> None:
        'Computes the cost of the entire portfolio'
        ammount = 0.
        for stock in self.stocks:
            ammount += stock.shares * stock.price
        return ammount

    def __repr__(self) -> str:
        return f"<{self.__class__.__name__} Cost: {self.cost()}>"


    
