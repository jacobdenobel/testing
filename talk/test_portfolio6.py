# test_portfolio6.py
import unittest
from portfolio import Portfolio


class PortfolioTestCase(unittest.TestCase):
    def assertCostEqual(self, p, cost):         
        self.assertEqual(p.cost(), cost)


class PortfolioTest(PortfolioTestCase):
    def test_empty(self):
        p = Portfolio()
        self.assertCostEqual(p, 0.0)
    
    def test_buy_one_stock(self):
        p = Portfolio()
        p.buy("Microsoft", 100, 50.0)
        self.assertCostEqual(p, 5000.0)

    def test_by_two_stocks(self):
        p = Portfolio()
        p.buy("Microsoft", 100, 50.0)
        p.buy("IBM", 40, 30.0)
        self.assertCostEqual(p, 6200.0)
  
   

if __name__ == "__main__":
    unittest.main()
