'''
Introduction:
    Why tests:
        - Know if you code works
        - Save time -> you don't have to fix problems later you didn't knew existed
        - Better code -> more modular, more readable
        - Remove fear -> If the tests work, your change probably hasn't broken anything
        - 'Debugging is hard, testing is easy'

    Testing is hard!
        - It is a lot of work
        - People find it tedious
        - But: it pays off! 

Unit testing:
    Real World Tests:
        - Can row quite large
        - Should be treated as real engineering

    Good tests:
        - Automated
        - Fast
        - Reliable
        - Informative
        - Focussed
    
    unittest module:
        - Python standard libary
        - Module for well structured tests  
    
References:    
    - https://docs.python.org/3/library/unittest.html
    - https://coverage.readthedocs.io
    - https://gitlab.com/jacobdenobel/testing/
'''
