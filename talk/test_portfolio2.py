# test_portfolio2.py
from portfolio import Portfolio

p = Portfolio()

print("The empty cost is {:.2f}, it should be 0.00".format(p.cost()))
p.buy("Microsoft", 100, 50.0)
print("With 100 Microsoft, at {:.2f} the cost is {:.2f}, it should be 5000.00".format(50.0, p.cost()))
p.buy("IBM", 40, 30.0)
print("With 40 IBM, at {:.2f} the cost is {:.2f}, it should be 6200.00".format(30.0, p.cost()))




