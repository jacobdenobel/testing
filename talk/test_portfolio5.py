# test_portfolio5.py
import unittest
from portfolio import Portfolio



class PortfolioTest(unittest.TestCase):
    def test_empty(self):
        p = Portfolio()
        self.assertEquals(p.cost(), 0.0)
    
    def test_buy_one_stock(self):
        p = Portfolio()
        p.buy("Microsoft", 100, 50.0)
        self.assertEquals(p.cost(), 5000.0)

    def test_by_two_stocks(self):
        p = Portfolio()
        p.buy("Microsoft", 100, 50.0)
        p.buy("IBM", 40, 30.0)
        self.assertEquals(p.cost(), 6200.0)
  
   

if __name__ == "__main__":
    unittest.main()
