# test_portfolio1.py
from portfolio import Portfolio

p = Portfolio()

print("The empty cost is", p.cost())
p.buy("Microsoft", 100, 50.0)
print("With 100 Microsoft, at {:.2f} the cost is {:.2f}".format(50.0, p.cost()))
p.buy("IBM", 40, 30.0)
print("With 40 IBM, at {:.2f} the cost is {:.2f}".format(30.0, p.cost()))





