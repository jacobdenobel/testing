#ssignment test_portfolio3.py
from portfolio import Portfolio

p = Portfolio()
assert p.cost() == 0.0

p.buy("Microsoft", 100, 50.0)
assert p.cost() == 5001.0

p.buy("IBM", 40, 30.0)
assert p.cost() == 6200.00



