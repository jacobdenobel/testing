# test_portfolio8.py
import unittest
from portfolio2 import Portfolio

class PortfolioTestCase(unittest.TestCase):
    def assertCostEqual(self, p, cost):         
        self.assertEqual(p.cost(), cost)

class PortfolioTest(PortfolioTestCase):
    def test_buy(self):
        p = Portfolio()
        p.buy("Microsoft", 100, 100.)
        p.buy("IBM", 40, 30.)
        p.buy("Google", 150, 99.99)
        self.assertCostEqual(p, 26198.5)
    
    def test_sell(self):
        p = Portfolio()
        p.buy("Microsoft", 100, 100.)
        p.buy("IBM", 40, 30.)
        p.buy("Google", 150, 99.99)
        p.sell('IBM', 3)
        self.assertCostEqual(p, 26108.5)
    

    def test_not_enough(self):
        p = Portfolio()
        p.buy("Microsoft", 100, 100.)
        p.buy("IBM", 40, 30.)
        p.buy("Google", 150, 99.99)
        with self.assertRaises(ValueError):
            p.sell("IBM", 100)

    def test_dont_own_stock(self):
        p = Portfolio()
        p.buy("Microsoft", 100, 100.)
        p.buy("IBM", 40, 30.)
        p.buy("Google", 150, 99.99)
        with self.assertRaises(ValueError):
            p.sell("NN Group", 100)



        

if __name__ == "__main__":
    unittest.main()
