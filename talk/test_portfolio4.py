# test_portfolio4.py
import unittest
from portfolio import Portfolio



class PortfolioTest(unittest.TestCase):
    def test_buy_one_stock(self):
        p = Portfolio()
        p.buy("Microsoft", 100, 50.0)
        assert p.cost() == 5000.0


def under_the_covers():
    '''
        This is performed for every method that
        starts with test_ on the TestCase    
    '''
    testcase = TestCase()
    try:
        testcase.test_<name>()
    except AssertionError as error:
        'Record failure' 
    else:
        'Record success'
        

       

if __name__ == "__main__":
    unittest.main()
